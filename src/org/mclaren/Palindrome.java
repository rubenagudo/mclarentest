package org.mclaren;

import java.util.*;

public class Palindrome {

    private static final Integer NUMBER_OF_PALINDROMES = 3;

    public List longestThreePalindromesInString(String string) {

        int L = string.length();
        Queue<String> queue = new PriorityQueue<>((o1, o2) -> o2.length() - o1.length()); //priority queue to have the longest strings on top
        Map<String, String> elementAndIndices = new HashMap<>();

        //generating all the substrings
        for (int i = 0; i < L; ++i) {
            for (int j = 0; j < (L - i); ++j) {
                String substring = string.substring(j, i + j + 1);
                queue.add(substring);
                elementAndIndices.put(substring, "Index: "+j+ ", Length: "+substring.length());
            }
        }

        List<String> results = new LinkedList<>();
        while(!queue.isEmpty()) {
            String element = queue.poll();
            if(isPalindrome(element)) {
                results.add("Text: " + element + ", " + elementAndIndices.get(element));
                if(results.size() == NUMBER_OF_PALINDROMES) {
                    //print and finish execution
                    printPalindromes(results);
                    return results;
                }
            }
        }
        return null;
    }

    private void printPalindromes(List<String> results) {
        results.forEach(System.out::println);
    }


    public boolean isPalindrome(String string) {
        int last = string.length()-1;
        for(int i = 0; i < string.length() / 2; i++) {
            if(string.charAt(i) != string.charAt(last-i)) return false;
        }

        return true;
    }

}
