# README #

### Instructions to make it work ###

1. Open the project with the IDE of your choice (I used IntelliJ)
2. Navigate to the test class ```PalindromeTest``` and run the tests
3. All of them should pass and you should be able to see the output in the console
