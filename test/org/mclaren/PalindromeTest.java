package org.mclaren;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class PalindromeTest {

    private Palindrome palindrome;

    @Before
    public void setUp() throws Exception {
        palindrome = new Palindrome();
    }

    @After
    public void tearDown() throws Exception {
        palindrome = null;
    }

    @Test
    public void testPalindromeWithThreePalindromes() {
        List result = palindrome.longestThreePalindromesInString("sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop");
        assert result.size() == 3;
    }

    @Test
    public void testPalindromeWithTwoPalindromes() {
        List result = palindrome.longestThreePalindromesInString("ab");
        assert result == null;
    }

    @Test
    public void isPalindromeFalse() {
        Boolean result = palindrome.isPalindrome("ab");
        assert !result;
    }

    @Test
    public void isPalindromeTrue() {
        Boolean result = palindrome.isPalindrome("aba");
        assert result;
    }
}
